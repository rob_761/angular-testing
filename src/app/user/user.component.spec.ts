/* tslint:disable:no-unused-variable */

import { TestBed, async, fakeAsync, tick } from '@angular/core/testing';
import { UserComponent } from './user.component';
import {UserService} from './user.service';
import {DataService} from '../shared/data.service';

describe('Component: User', () => {
  beforeEach(() => { // callback
    TestBed.configureTestingModule({
      declarations: [UserComponent]
    });
  })

  it('should create the app', () => {
    const fixture = TestBed.createComponent(UserComponent); // create component in test environment
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy(); // does it exist
  });

  it ('should use the user name from the service', () => {
    const fixture = TestBed.createComponent(UserComponent); // create component in test environment
    const app = fixture.debugElement.componentInstance;
    const userService = fixture.debugElement.injector.get(UserService); // use angular injector to get instance ot UserService
    fixture.detectChanges(); // must detect changes after update
    expect(userService.user.name).toEqual(app.user.name);
  });

  it ('should use the user name if user is logged in', () => {
    const fixture = TestBed.createComponent(UserComponent); // create component in test environment
    const app = fixture.debugElement.componentInstance;
    app.isLoggedIn = true;
    fixture.detectChanges(); // must detect changes after update
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('p').textContent).toContain(app.user.name);
  });

  it ('shouldn\'t use the user name if user is not logged in', () => {
    const fixture = TestBed.createComponent(UserComponent); // create component in test environment
    const app = fixture.debugElement.componentInstance;
    app.isLoggedIn = false;
    fixture.detectChanges(); // must detect changes after update
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('p').textContent).not.toContain(app.user.name);
  });

  it('shouldnt fetch data successfully if not called asynchronously', () => {
    const fixture = TestBed.createComponent(UserComponent); // create component in test environment
    const app = fixture.debugElement.componentInstance;
    const dataService = fixture.debugElement.injector.get(DataService);
    const spy = spyOn(dataService, 'getDetails') // listen on getDetails
      .and.returnValue(Promise.resolve('Data')); // execute async code and return Data
    fixture.detectChanges();
    expect(app.data).toBe(undefined); // initial value is undefined
  });

  it('should fetch data successfully if called asynchronously', async(() => { // wrap callback function with async to create fake async env
    const fixture = TestBed.createComponent(UserComponent); // create component in test environment
    const app = fixture.debugElement.componentInstance;
    const dataService = fixture.debugElement.injector.get(DataService);
    const spy = spyOn(dataService, 'getDetails') // listen on getDetails
      .and.returnValue(Promise.resolve('Data')); // execute async code and return Data
    fixture.detectChanges();
    fixture.whenStable().then(() => { // react when async task is finished
      expect(app.data).toBe('Data');
    });
  }));

  it('should fetch data successfully if called asynchronously', fakeAsync(() => {
    const fixture = TestBed.createComponent(UserComponent); // create component in test environment
    const app = fixture.debugElement.componentInstance;
    const dataService = fixture.debugElement.injector.get(DataService);
    const spy = spyOn(dataService, 'getDetails') // listen on getDetails
      .and.returnValue(Promise.resolve('Data')); // execute async code and return Data
    fixture.detectChanges();
    tick(); // finish all fake async tasks
    expect(app.data).toBe('Data');
    }));
});
